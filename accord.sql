-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: mysql
-- Generation Time: 22-Ago-2019 às 12:58
-- Versão do servidor: 5.7.22
-- PHP Version: 7.2.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `accord`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `aulas`
--

CREATE TABLE `aulas` (
  `id` int(10) UNSIGNED NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `palestrante_id` int(10) UNSIGNED DEFAULT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `descricao` text COLLATE utf8_unicode_ci NOT NULL,
  `video` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `aulas_questoes`
--

CREATE TABLE `aulas_questoes` (
  `id` int(10) UNSIGNED NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `aula_id` int(10) UNSIGNED DEFAULT NULL,
  `questao` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `aulas_questoes_alternativas`
--

CREATE TABLE `aulas_questoes_alternativas` (
  `id` int(10) UNSIGNED NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `questao_id` int(10) UNSIGNED DEFAULT NULL,
  `alternativa` text COLLATE utf8_unicode_ci NOT NULL,
  `alternativa_correta` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `cadastros`
--

CREATE TABLE `cadastros` (
  `id` int(10) UNSIGNED NOT NULL,
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `registro_funcional_tipo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `registro_funcional_uf` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `registro_funcional` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `especialidade` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cargo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `empresa` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `senha` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `cadastro_aula`
--

CREATE TABLE `cadastro_aula` (
  `id` int(10) UNSIGNED NOT NULL,
  `cadastro_id` int(10) UNSIGNED NOT NULL,
  `aula_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `cadastro_questao_resposta`
--

CREATE TABLE `cadastro_questao_resposta` (
  `id` int(10) UNSIGNED NOT NULL,
  `cadastro_id` int(10) UNSIGNED NOT NULL,
  `questao_id` int(10) UNSIGNED NOT NULL,
  `alternativa_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `configuracoes`
--

CREATE TABLE `configuracoes` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `keywords` text COLLATE utf8_unicode_ci NOT NULL,
  `imagem_de_compartilhamento` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `analytics` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `configuracoes`
--

INSERT INTO `configuracoes` (`id`, `title`, `description`, `keywords`, `imagem_de_compartilhamento`, `analytics`, `created_at`, `updated_at`) VALUES
(1, 'Accord Academy', '', '', '', '', NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `contato`
--

CREATE TABLE `contato` (
  `id` int(10) UNSIGNED NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `termos_rodape` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `contato`
--

INSERT INTO `contato` (`id`, `email`, `telefone`, `termos_rodape`, `created_at`, `updated_at`) VALUES
(1, 'sac@accordfarma.com.br', '11 5678 1234', 'Termos e condições de uso do site.<br>Lorem ipsum dolor sit, consectetur adipiscing elit. Pellentesque malesuada eleifend nisl, sed tristique eros sagittis quis. Donec nibh turpis, feugiat ac sapien ac, sagittis interdum lectus.', NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `contatos_recebidos`
--

CREATE TABLE `contatos_recebidos` (
  `id` int(10) UNSIGNED NOT NULL,
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mensagem` text COLLATE utf8_unicode_ci NOT NULL,
  `lido` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2016_02_01_000000_create_users_table', 1),
('2016_03_01_000000_create_contato_table', 1),
('2016_03_01_000000_create_contatos_recebidos_table', 1),
('2017_09_01_163723_create_configuracoes_table', 1),
('2019_04_05_001245_create_sobre_o_programa_table', 1),
('2019_04_05_004136_create_palestrantes_table', 1),
('2019_04_05_004437_create_aulas_table', 1),
('2019_04_05_013519_create_cadastros_table', 1),
('2019_04_05_021235_create_password_resets_table', 1),
('2019_04_22_175731_create_aulas_questoes_table', 1),
('2019_04_22_185728_create_cadastro_questao_resposta_table', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `palestrantes`
--

CREATE TABLE `palestrantes` (
  `id` int(10) UNSIGNED NOT NULL,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `apresentacao` text COLLATE utf8_unicode_ci NOT NULL,
  `foto` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `password_resets`
--

CREATE TABLE `password_resets` (
  `id` int(10) UNSIGNED NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `sobre_o_programa`
--

CREATE TABLE `sobre_o_programa` (
  `id` int(10) UNSIGNED NOT NULL,
  `apresentacao` text COLLATE utf8_unicode_ci NOT NULL,
  `titulo` text COLLATE utf8_unicode_ci NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `sobre_o_programa`
--

INSERT INTO `sobre_o_programa` (`id`, `apresentacao`, `titulo`, `texto`, `created_at`, `updated_at`) VALUES
(1, 'Lorem ipsum dolor sit amet<br>consectetur adipiscing elit', 'TÍTULO E TEXTO A RESPEITO<br>DO PROGRAMA', '<p>Lorem ipsum dolor sit, consectetur adipiscing elit. Pellentesque malesuada eleifend nisl, sed tristique eros sagittis quis. Donec nibh turpis, feugiat ac sapien ac, sagittis interdum lectus. Ut pretium sollicitudin risus sit amet vehicula. Nunc egestas mauris lorem, nec fringilla elit commodo sit amet. Cras ornare nibh facilisis nibh lacinia, eu pellentesque nulla auctor. Nunc in pretium nibh, facilisis faucibus arcu. In interdum nunc eu libero finibus feugiat. Quisque mollis lorem ac velit malesuada tristique eu at tellus. Donec purus elit, sollicitudin nec laoreet quis, blandit eget sem. Cras metus nunc, condimentum a volutpat non, condimentum ut justo.</p><p>Pellentesque at dolor ornare, facilisis lectus ut, pretium nunc. Sed dignissim sed augue at ornare. Proin sed odio non erat ornare feugiat. Nam sollicitudin quis ligula lacinia ultricies. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Aenean nisi mi, luctus eget est et, rhoncus molestie quam. Donec in leo eu ante volutpat posuere nec sit amet erat.</p>', NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'trupe', 'contato@trupe.net', '$2y$10$ay4rVVFWqCzUF20amjqInOxry6dJpKab5UTqw1Fye91R.XkjflMnC', NULL, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `aulas`
--
ALTER TABLE `aulas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `aulas_palestrante_id_foreign` (`palestrante_id`);

--
-- Indexes for table `aulas_questoes`
--
ALTER TABLE `aulas_questoes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `aulas_questoes_aula_id_foreign` (`aula_id`);

--
-- Indexes for table `aulas_questoes_alternativas`
--
ALTER TABLE `aulas_questoes_alternativas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `aulas_questoes_alternativas_questao_id_foreign` (`questao_id`);

--
-- Indexes for table `cadastros`
--
ALTER TABLE `cadastros`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `cadastros_email_unique` (`email`);

--
-- Indexes for table `cadastro_aula`
--
ALTER TABLE `cadastro_aula`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cadastro_aula_cadastro_id_foreign` (`cadastro_id`),
  ADD KEY `cadastro_aula_aula_id_foreign` (`aula_id`);

--
-- Indexes for table `cadastro_questao_resposta`
--
ALTER TABLE `cadastro_questao_resposta`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cadastro_questao_resposta_cadastro_id_foreign` (`cadastro_id`),
  ADD KEY `cadastro_questao_resposta_questao_id_foreign` (`questao_id`),
  ADD KEY `cadastro_questao_resposta_alternativa_id_foreign` (`alternativa_id`);

--
-- Indexes for table `configuracoes`
--
ALTER TABLE `configuracoes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contato`
--
ALTER TABLE `contato`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contatos_recebidos`
--
ALTER TABLE `contatos_recebidos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `palestrantes`
--
ALTER TABLE `palestrantes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD PRIMARY KEY (`id`),
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `sobre_o_programa`
--
ALTER TABLE `sobre_o_programa`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `aulas`
--
ALTER TABLE `aulas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `aulas_questoes`
--
ALTER TABLE `aulas_questoes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `aulas_questoes_alternativas`
--
ALTER TABLE `aulas_questoes_alternativas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cadastros`
--
ALTER TABLE `cadastros`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cadastro_aula`
--
ALTER TABLE `cadastro_aula`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cadastro_questao_resposta`
--
ALTER TABLE `cadastro_questao_resposta`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `configuracoes`
--
ALTER TABLE `configuracoes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `contato`
--
ALTER TABLE `contato`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `contatos_recebidos`
--
ALTER TABLE `contatos_recebidos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `palestrantes`
--
ALTER TABLE `palestrantes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `password_resets`
--
ALTER TABLE `password_resets`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sobre_o_programa`
--
ALTER TABLE `sobre_o_programa`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `aulas`
--
ALTER TABLE `aulas`
  ADD CONSTRAINT `aulas_palestrante_id_foreign` FOREIGN KEY (`palestrante_id`) REFERENCES `palestrantes` (`id`) ON DELETE SET NULL;

--
-- Limitadores para a tabela `aulas_questoes`
--
ALTER TABLE `aulas_questoes`
  ADD CONSTRAINT `aulas_questoes_aula_id_foreign` FOREIGN KEY (`aula_id`) REFERENCES `aulas` (`id`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `aulas_questoes_alternativas`
--
ALTER TABLE `aulas_questoes_alternativas`
  ADD CONSTRAINT `aulas_questoes_alternativas_questao_id_foreign` FOREIGN KEY (`questao_id`) REFERENCES `aulas_questoes` (`id`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `cadastro_aula`
--
ALTER TABLE `cadastro_aula`
  ADD CONSTRAINT `cadastro_aula_aula_id_foreign` FOREIGN KEY (`aula_id`) REFERENCES `aulas` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `cadastro_aula_cadastro_id_foreign` FOREIGN KEY (`cadastro_id`) REFERENCES `cadastros` (`id`) ON DELETE CASCADE;

--
-- Limitadores para a tabela `cadastro_questao_resposta`
--
ALTER TABLE `cadastro_questao_resposta`
  ADD CONSTRAINT `cadastro_questao_resposta_alternativa_id_foreign` FOREIGN KEY (`alternativa_id`) REFERENCES `aulas_questoes_alternativas` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `cadastro_questao_resposta_cadastro_id_foreign` FOREIGN KEY (`cadastro_id`) REFERENCES `cadastros` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `cadastro_questao_resposta_questao_id_foreign` FOREIGN KEY (`questao_id`) REFERENCES `aulas_questoes` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
