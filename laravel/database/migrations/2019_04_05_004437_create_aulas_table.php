<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAulasTable extends Migration
{
    public function up()
    {
        Schema::create('aulas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ordem')->default(0);
            $table->string('slug');
            $table->integer('palestrante_id')->unsigned()->nullable();
            $table->foreign('palestrante_id')->references('id')->on('palestrantes')->onDelete('set null');
            $table->string('titulo');
            $table->integer('modulo');
            $table->string('carga_horaria');
            $table->text('descricao');
            $table->string('capa');
            $table->string('video');
            $table->string('certificado');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('aulas');
    }
}
