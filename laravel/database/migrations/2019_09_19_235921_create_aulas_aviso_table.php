<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAulasAvisoTable extends Migration
{
    public function up()
    {
        Schema::create('aulas_aviso', function (Blueprint $table) {
            $table->increments('id');
            $table->text('aviso');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('aulas_aviso');
    }
}
