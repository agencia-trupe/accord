@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Aulas /</small> Editar Aula</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.aulas.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.aulas.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
