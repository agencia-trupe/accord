@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('alternativa', 'Alternativa') !!}
    {!! Form::text('alternativa', null, ['class' => 'form-control']) !!}
</div>

<div class="checkbox form-group">
    <label>
        <input type="hidden" name="alternativa_correta" value="0">
        {!! Form::checkbox('alternativa_correta', 1) !!}
        <span style="font-weight:bold">Alternativa correta</span>
    </label>
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.aulas.questoes.alternativas.index', [$aula->id, $questao->id]) }}" class="btn btn-default btn-voltar">Voltar</a>
