@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Aulas / {{ $aula->titulo }} /</small> Adicionar Questão</h2>
    </legend>

    {!! Form::open(['route' => ['painel.aulas.questoes.store', $aula->id]]) !!}

        @include('painel.aulas.questoes.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
