@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('questao', 'Questão') !!}
    {!! Form::text('questao', null, ['class' => 'form-control']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}

<a href="{{ route('painel.aulas.questoes.index', $aula->id) }}" class="btn btn-default btn-voltar">Voltar</a>
