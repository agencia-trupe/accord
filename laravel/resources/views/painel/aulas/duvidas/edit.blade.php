@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Aulas / {{ $aula->titulo }} /</small> {{ $registro->resposta ? 'Editar' : 'Responder' }} dúvida</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.aulas.duvidas.update', $aula->id, $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.aulas.duvidas.form', ['submitText' => 'Salvar'])

    {!! Form::close() !!}

@endsection
