@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('apresentacao', 'Apresentação') !!}
    {!! Form::textarea('apresentacao', null, ['class' => 'form-control ckeditor', 'data-editor' => 'apresentacao']) !!}
</div>

<hr>

<div class="form-group">
    {!! Form::label('titulo', 'Título') !!}
    {!! Form::textarea('titulo', null, ['class' => 'form-control ckeditor', 'data-editor' => 'cleanBr']) !!}
</div>

<div class="form-group">
    {!! Form::label('texto', 'Texto') !!}
    {!! Form::textarea('texto', null, ['class' => 'form-control ckeditor', 'data-editor' => 'completo']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
