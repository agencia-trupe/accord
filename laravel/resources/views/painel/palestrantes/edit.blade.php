@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Palestrantes /</small> Editar Palestrante</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.palestrantes.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.palestrantes.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
