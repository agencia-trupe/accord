@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Palestrantes /</small> Adicionar Palestrante</h2>
    </legend>

    {!! Form::open(['route' => 'painel.palestrantes.store', 'files' => true]) !!}

        @include('painel.palestrantes.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
