@extends('frontend.common.template')

@section('content')

    <div class="main certificado">
        <div class="center">
            <div class="wrapper">
                <h1>EMITA SEU CERTIFICADO AQUI</h1>
                <p>Para emitir o certificado de cada módulo é preciso ter assistido a aula e ter ao menos 75% de respostas corretas no Teste de Conhecimentos.</p>

                @foreach($aulas as $aula)
                <div class="certificado-aula">
                    <div class="numero">
                        <span>{{ sprintf("%02d", $aula->modulo) }}</span>
                    </div>
                    <div class="texto">
                        <h4>MÓDULO {{ Tools::numToRoman($aula->modulo) }}</h4>
                        <h3>{{ $aula->titulo }}</h3>
                        @if(! $aula->respondida)
                            <p>Você ainda não completou a aula e/ou o teste de Conhecimentos</p>
                        @elseif($aula->aptaAoCertificado)
                            <p>Você cumpriu os requisitos para emissão do certificado</p>
                            <a href="{{ route('certificado.emissao', $aula->slug) }}">GERAR CERTIFICADO</a>
                        @else
                            <p class="invalido">Você não atingiu os requisitos mínimos para emissão do Certificado deste módulo. Por gentileza, retorne após o prazo de 72 horas para refazer o teste de conhecimento. Agradecemos a participação.</p>
                        @endif
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>

@endsection
