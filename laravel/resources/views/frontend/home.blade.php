@extends('frontend.common.template')

@section('content')

    <div class="main sobre">
        <div class="center">
            <div class="texto">
                {!! $apresentacao !!}
            </div>
        </div>
    </div>

@endsection
