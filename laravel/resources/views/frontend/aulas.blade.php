@extends('frontend.common.template')

@section('content')

    <div class="main aulas">
        <div class="center">
            <h1>AULAS</h1>

            @if($aviso)
            <div class="aulas-aviso">
                <p>{{ $aviso }}</p>
            </div>
            @endif

            <div class="aulas-lista">
                @foreach($aulas as $a)
                <a href="{{ route('aulas.show', $a->slug) }}">
                    <div class="aula-capa">
                        <img src="{{ asset('assets/img/aulas/'.$a->capa) }}" alt="">
                        <div class="numero">
                            <span>{{ sprintf("%02d", $a->modulo) }}</span>
                        </div>
                    </div>
                    <div class="aula-texto">
                        <h2>{{ $a->titulo }}</h2>
                        <h3>{{ $a->palestrante->nome }}</h3>
                    </div>
                </a>
                @endforeach
            </div>
        </div>
    </div>

@endsection
