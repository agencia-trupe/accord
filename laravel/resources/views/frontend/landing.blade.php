@extends('frontend.common.template')

@section('content')

<div class="main landing">
    <div class="center">
        <section id="home" class="landing-chamada">
            <h3>Programas de Atualização para Profissionais de Saúde</h3>
            <p>Temas relevantes sob aspectos técnicos e de qualidade.</p>

            <div class="chamadas">
                <a href="#sobre" class="nav-scroll chamada">
                    <h4>PAAF FARMACÊUTICOS</h4>
                    <p>Atualização técnica para farmacêuticos em oncologia, com chancela da SOBRAFO</p>
                    <span class="seta"></span>
                </a>
                <a href="{{ url('/enfermagem') }}" class="chamada">
                    <h4>PAAF ENFERMAGEM</h4>
                    <p>Atualização técnica para enfermeiros em oncologia, com chancela da ABRENFOH</p>
                    <span class="seta"></span>
                </a>
                <a href="{{ url('/nefrologia') }}" class="chamada">
                    <h4>NEFROLOGIA</h4>
                    <p>EVENTO
                        </br>Cadastre-se já para o evento ao vivo do dia 01/12/2021.
                        </br>Clique aqui ></p>
                    <span class="seta"></span>
                </a>
            </div>
        </section>

        <section id="sobre" class="landing-sobre">
            <h2>SOBRE</h2>

            <h3>Desenvolvimento Profissional Contínuo</h3>
            <p>Conectando o profissional de saúde a conteúdo de qualidade.</p>

            <div class="caixas">
                <div class="caixa">
                    <h4>ACESSO À INFORMAÇÃO</h4>
                    <p>O conteúdo que você precisa, acessível a qualquer momento, de qualquer lugar e gratuitamente.</p>
                </div>
                <div class="caixa">
                    <h4>CERTIFICADOS RECONHECIDOS EM TODO BRASIL</h4>
                    <p>Cursos com chancelas da SOBRAFO, SBRAFH e ABRENFOH</p>
                </div>
                <div class="caixa">
                    <h4>TROQUE EXPERIÊNCIAS</h4>
                    <p>Participe dos fóruns de discussão dos temas abordados nos módulos</p>
                </div>
            </div>

            <h3>Sobre o Accord Academy</h3>
            <p>Está dividido em três programas:</p>
            <p>
                <strong>PAAF – Programa de Atualização Accord Farmacêutica</strong><br>
                Ensino à distância (EAD), o PAAF disponibiliza atualização técnica por meio de uma plataforma educacional que pode ser acessada em qualquer lugar e horário, proporcionando a profissionais de saúde, como farmacêuticos e enfermeiros, contato a conteúdos exclusivos e de qualidade, chancelados e certificados pelas principais sociedades de saúde e acumulando pontos para creditação de títulos.
            </p>
            <p>
                <strong>Expert Meetings</strong><br>
                Encontros entre profissionais da área da saúde e experts em assuntos de grande relevância, acontecem em diversas cidades do Brasil em formato intimista. A presença do especialista propõe o aprofundamento dos temas abordados e interação entre os profissionais de saúde seniors.
            </p>
            <p>
                <strong>Accord Connect</strong><br>
                É um programa voltado para o público interno da Accord, entre colaboradores e parceiros, com o objetivo de engajar e transformar as vidas dessas pessoas, dando poder, praticidade e valorizando a carreira e o conhecimento. Frequentemente convidamos experts para compartilhar e treinar suas experiências com nossos colaboradores.
            </p>
            <h3>Sobre a Accord Farmacêutica</h3>
            <p>
                Há 10 anos atendendo o Brasil, a Accord Farmacêutica faz parte do Grupo INTAS, empresa presente em mais de 85 países. Tem em seu portfólio produtos de oncologia e terapias críticas, contribuindo para que mais pacientes tenham acesso a medicamentos de alta complexidade e tecnologia, com qualidade e segurança.
            </p>
            <p>
                A Accord Farmacêutica apoia iniciativas relevantes para uma sociedade nas áreas culturais, científicas, educacionais e de sustentabilidade. Assim, criou-se a Accord Academy, primeira iniciativa educacional própria da marca.
            </p>
            <p>
                Para conhecer melhor a Accord, acesse o nosso site e entre em contato
            </p>
            <p><a href="http://www.accordfarma.com.br">www.accordfarma.com.br</a></p>
        </section>

        <section id="contato" class="landing-contato">
            <h2>CONTATO</h2>

            <div class="row">
                <div class="col">
                    <p class="email">
                        <a href="mailto:oi@accordfarma.com.br">oi@accordfarma.com.br</a>
                    </p>
                    <p class="sac">SAC 0800 723 9777</p>
                    <p class="whatsapp">+55 11 99115 2017</p>
                </div>
                <div class="col">
                    <p class="link">
                        <a href="http://www.accordfarma.com.br">accordfarma.com.br</a>
                    </p>
                    <p class="link">
                        <a href="http://www.accordacademy.com.br">accordacademy.com.br</a>
                    </p>
                </div>
            </div>
        </section>

        <section id="login" class="login">
            <h2>ENTRE OU CADASTRE-SE</h2>
            <h3>Você precisa estar logado para acessar o conteúdo.</h3>
            <div class="grid">
                <a href="{{ route('loginCadastro') }}" class="btn-login">
                    <span>PERFIL</span>
                    FARMACÊUTICOS
                </a>
                <a href="{{ route('cadastro') }}" class="btn-cadastro">
                    AINDA NÃO TENHO CADASTRO.<br>
                    CADASTRAR PERFIL FARMACÊUTICOS >
                </a>
                <a href="{{ url('/enfermagem/login') }}" class="btn-login">
                    <span>PERFIL</span>
                    ENFERMAGEM
                </a>
                <a href="{{ url('/enfermagem/cadastro') }}" class="btn-cadastro">
                    AINDA NÃO TENHO CADASTRO.<br>
                    CADASTRAR PERFIL ENFERMAGEM >
                </a>
                <a href="{{ url('/nefrologia/login') }}" class="btn-login">
                    <span>PERFIL</span>
                    NEFROLOGIA
                </a>
                <a href="{{ url('/nefrologia/cadastro') }}" class="btn-cadastro">
                    AINDA NÃO TENHO CADASTRO.<br>
                    CADASTRAR PERFIL NEFROLOGIA >
                </a>
            </div>
        </section>
    </div>
</div>

@endsection