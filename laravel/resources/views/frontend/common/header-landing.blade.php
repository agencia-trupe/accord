    @if(!Tools::routeIs('home'))
    <div class="barra-perfil">
        <div class="center">
            PERFIL FARMACÊUTICOS
        </div>
    </div>
    @endif

    <header class="header-landing @if(Tools::routeIs('home')) header-fixed @endif">
        <div class="center">
            <div class="wrapper">
                @if(Tools::routeIs('home'))
                <a href="#home" class="logo nav-scroll">
                    <img src="{{ asset('assets/img/layout/marca-accordacademy.png') }}" alt="{{ config('app.name') }}">
                </a>
                <nav>
                    <a href="#sobre" class="nav-scroll">SOBRE</a>
                    <a href="#contato" class="nav-scroll">CONTATO</a>
                    <a href="#login" class="nav-scroll">ENTRE OU CADASTRE-SE</a>
                </nav>
                @else
                <a href="{{ route('home') }}" class="logo">
                    <img src="{{ asset('assets/img/layout/marca-accordacademy.png') }}" alt="{{ config('app.name') }}">
                </a>
                <nav>
                    <a href="{{ route('home') }}#sobre">SOBRE</a>
                    <a href="{{ route('home') }}#contato">CONTATO</a>
                    <a href="{{ route('home') }}#login">ENTRE OU CADASTRE-SE</a>
                </nav>
                @endif
            </div>
        </div>
    </header>
