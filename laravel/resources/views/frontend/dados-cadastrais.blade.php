@extends('frontend.common.template')

@section('content')

    <div class="main login">
        <div class="center">
            <div class="wrapper">
                <p>EDITAR CADASTRO</p>

                <form action="{{ route('dadosPost') }}" class="form-padrao form-cadastro" method="POST">
                    <input type="hidden" name="_method" value="PATCH">
                    @if($errors->any())
                        <div class="erro">
                            @foreach($errors->all() as $error)
                            {{ $error }}<br>
                            @endforeach
                        </div>
                    @endif
                    @if(session('success'))
                        <div class="sucesso">
                            {{ session('success') }}
                        </div>
                    @endif

                    {!! csrf_field() !!}

                    <div class="row">
                        <label for="nome">NOME COMPLETO</label>
                        <input type="text" name="nome" id="nome" value="{{ old('nome') ?: $user->nome }}" required>
                    </div>
                    <div class="row">
                        <label for="email">E-MAIL</label>
                        <input type="email" name="email" id="email" value="{{ old('email') ?: $user->email }}" required>
                    </div>
                    <div class="row">
                        <label for="registro_funcional_tipo" class="lg">TIPO DE REGISTRO FUNCIONAL</label>
                        <input type="text" name="registro_funcional_tipo" id="registro_funcional_tipo" value="{{ old('registro_funcional_tipo') ?: $user->registro_funcional_tipo }}" required>
                    </div>
                    <div class="row">
                        <label for="registro_funcional_uf" class="lg">UF DO NÚMERO DE REGISTRO FUNCIONAL</label>
                        <select name="registro_funcional_uf" id="registro_funcional_uf" required>
                            <option value="">Selecione</option>
                            @foreach(Tools::listaUf() as $uf)
                            <option value="{{ $uf }}" @if($user->registro_funcional_uf == $uf) selected @endif>{{ $uf }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="row">
                        <label for="registro_funcional">REGISTRO FUNCIONAL</label>
                        <input type="text" name="registro_funcional" id="registro_funcional" value="{{ old('registro_funcional') ?: $user->registro_funcional }}" required>
                    </div>
                    <div class="row">
                        <label for="especialidade">ESPECIALIDADE</label>
                        <input type="text" name="especialidade" id="especialidade" value="{{ old('especialidade') ?: $user->especialidade }}" required>
                    </div>
                    <div class="row">
                        <label for="cargo">CARGO</label>
                        <input type="text" name="cargo" id="cargo" value="{{ old('cargo') ?: $user->cargo }}" required>
                    </div>
                    <div class="row">
                        <label for="empresa">EMPRESA</label>
                        <input type="text" name="empresa" id="empresa" value="{{ old('empresa') ?: $user->empresa }}" required>
                    </div>
                    <div class="row">
                        <label for="senha">NOVA SENHA</label>
                        <input type="password" name="senha" id="senha">
                    </div>
                    <div class="row">
                        <label for="senha_confirmation">REPETIR SENHA</label>
                        <input type="password" name="senha_confirmation" id="senha_confirmation">
                    </div>

                    <input type="submit" value="SALVAR">
                </form>
            </div>
        </div>
    </div>

@endsection
