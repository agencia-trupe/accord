<!DOCTYPE html>
<html>
<head>
    <title>Recuperação de senha</title>
    <meta charset="utf-8">
</head>
<body>
    <p>
        Olá,<br>
        Você solicitou recuperação da sua senha em nosso sistema. As senhas são criptografadas e seguras, portanto você deve criar uma nova senha acessando o link:<br>
        <a href="{{ url('redefinicao-de-senha', $token).'?email='.urlencode($user->getEmailForPasswordReset()) }}">Clique aqui para redefinir sua senha.</a>
    </p>
</body>
</html>
