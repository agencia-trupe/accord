<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class AulasDuvidasRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'duvida' => 'required',
            'resposta' => 'required'
        ];

        if ($this->method() != 'POST') {
        }

        return $rules;
    }

    public function attributes()
    {
        return [
            'duvida' => 'dúvida'
        ];
    }
}
