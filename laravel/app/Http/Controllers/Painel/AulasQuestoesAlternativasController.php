<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\AulasQuestoesAlternativasRequest;
use App\Http\Controllers\Controller;

use App\Models\Aula;
use App\Models\AulaQuestao;
use App\Models\AulaQuestaoAlternativa;

class AulasQuestoesAlternativasController extends Controller
{
    public function index(Aula $aula, AulaQuestao $questao)
    {
        $registros = $questao->alternativas;

        return view('painel.aulas.questoes.alternativas.index', compact('aula', 'questao', 'registros'));
    }

    public function create(Aula $aula, AulaQuestao $questao)
    {
        return view('painel.aulas.questoes.alternativas.create', compact('aula', 'questao'));
    }

    public function store(AulasQuestoesAlternativasRequest $request, Aula $aula, AulaQuestao $questao)
    {
        try {

            $input = $request->all();

            $questao->alternativas()->create($input);

            return redirect()->route('painel.aulas.questoes.alternativas.index', [$aula->id, $questao->id])->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Aula $aula, AulaQuestao $questao, AulaQuestaoAlternativa $registro)
    {
        return view('painel.aulas.questoes.alternativas.edit', compact('aula', 'questao', 'registro'));
    }

    public function update(AulasQuestoesAlternativasRequest $request, Aula $aula, AulaQuestao $questao, AulaQuestaoAlternativa $registro)
    {
        try {

            $input = $request->all();

            $registro->update($input);

            return redirect()->route('painel.aulas.questoes.alternativas.index', [$aula->id, $questao->id])->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Aula $aula, AulaQuestao $questao, AulaQuestaoAlternativa $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.aulas.questoes.alternativas.index', [$aula->id, $questao->id])->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }
}
