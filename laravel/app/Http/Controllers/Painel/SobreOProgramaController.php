<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\SobreOProgramaRequest;
use App\Http\Controllers\Controller;

use App\Models\SobreOPrograma;

class SobreOProgramaController extends Controller
{
    public function index()
    {
        $registro = SobreOPrograma::first();

        return view('painel.sobre-o-programa.edit', compact('registro'));
    }

    public function update(SobreOProgramaRequest $request, SobreOPrograma $registro)
    {
        try {
            $input = $request->all();


            $registro->update($input);

            return redirect()->route('painel.sobre-o-programa.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
