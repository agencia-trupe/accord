<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\AulasQuestoesRequest;
use App\Http\Controllers\Controller;

use App\Models\Aula;
use App\Models\AulaQuestao;

class AulasQuestoesController extends Controller
{
    public function index(Aula $aula)
    {
        $registros = $aula->questoes;

        return view('painel.aulas.questoes.index', compact('aula', 'registros'));
    }

    public function create(Aula $aula)
    {
        return view('painel.aulas.questoes.create', compact('aula'));
    }

    public function store(AulasQuestoesRequest $request, Aula $aula)
    {
        try {

            $input = $request->all();

            $aula->questoes()->create($input);

            return redirect()->route('painel.aulas.questoes.index', $aula->id)->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Aula $aula, AulaQuestao $registro)
    {
        return view('painel.aulas.questoes.edit', compact('aula', 'registro'));
    }

    public function update(AulasQuestoesRequest $request, Aula $aula, AulaQuestao $registro)
    {
        try {

            $input = $request->all();

            $registro->update($input);

            return redirect()->route('painel.aulas.questoes.index', $aula->id)->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Aula $aula, AulaQuestao $registro)
    {
        try {

            $registro->delete();

            return redirect()->route('painel.aulas.questoes.index', $aula->id)->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }
}
