<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CadastroQuestaoResposta extends Model
{
    protected $table = 'cadastro_questao_resposta';

    protected $guarded = ['id'];

    public function scopeQuestao($query, $id)
    {
        return $query->where('questao_id', $id);
    }

    public function questao()
    {
        return $this->hasOne(AulaQuestao::class, 'id', 'questao_id');
    }

    public function alternativa()
    {
        return $this->hasOne(AulaQuestaoAlternativa::class, 'id', 'alternativa_id');
    }
}
