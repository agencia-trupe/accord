<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AulaQuestaoAlternativa extends Model
{
    protected $table = 'aulas_questoes_alternativas';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'ASC');
    }
}
